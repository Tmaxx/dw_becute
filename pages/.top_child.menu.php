<?php
$aMenuLinks = [
    [
        "About Us",
        "/about_us/",
        [],
        [],
        "",
    ],
    [
        "Contact Us",
        "/contact/",
        [],
        [],
        "",
    ],
    [
        "Cart Page",
        "/cart/",
        [],
        [],
        "",
    ],
    [
        "Portfolio Page",
        "/",
        [],
        [],
        "",
    ],
    [
        "Wishlist Page",
        "/",
        [],
        [],
        "",
    ],
    [
        "Privacy Policy",
        "/",
        [],
        [],
        "",
    ],
    [
        "Login Page",
        "/",
        [],
        [],
        "",
    ],
    [
        "Error Page",
        "/",
        [],
        [],
        "",
    ],
];
