<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<?php if ($arResult["ITEMS"]): ?>
    <section class="testimonial__section testimonial__bg section--padding">
        <div class="container">
            <div class="section__heading text-center mb-40">
                <h2 class="section__heading--maintitle"><?php $APPLICATION->IncludeComponent("bitrix:main.include", "", Array(
                            "AREA_FILE_SHOW" => "file",	// Показывать включаемую область
                            "PATH" => SITE_TEMPLATE_PATH."/includes/reviews_title.php",	// Путь к файлу области
                        )
                    );?>
                </h2>
            </div>
            <div class="testimonial__section--inner testimonial__swiper--activation swiper">

                        <div class="swiper-wrapper">
                            <?foreach($arResult["ITEMS"] as $arItem):?>

                                <div class="swiper-slide">

                                        <div class="testimonial__items">
                                            <div class="testimonial__author d-flex align-items-center">
                                                <div class="testimonial__author__thumbnail">
                                                    <img src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>" alt="testimonial-img">
                                                </div>
                                                <div class="testimonial__author--text">
                                                    <h3 class="testimonial__author--title"><?= $arItem["NAME"] ?? ''; ?></h3>
                                                    <span class="testimonial__author--subtitle"><?= $arItem["DISPLAY_PROPERTIES"]['SUBTITLE']['VALUE'] ?? ''; ?></span>
                                                    <?= $arItem['PREVIEW_TEXT'] ?? ''; ?>
                                                </div>
                                            </div>
                                            <div class="testimonial__content">
                                                <p class="testimonial__desc">
                                                    <?= $arItem['DETAIL_TEXT'] ?? ''; ?></p>
                                                <img class="testimonial__vector--icon" src="<?= SITE_TEMPLATE_PATH ?>/assets/img/icon/vector-icon.webp" alt="icon">
                                            </div>

                                        </div>
                                </div>
                            <?php endforeach; ?>
                        </div>

            <div class="testimonial__pagination swiper-pagination"></div>
            </div>
        </div>
    </section>
<?php endif; ?>