<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<!-- Start banner section -->
<?php if ($arResult["ITEMS"]): ?>
    <section class="banner__section section--padding pt-0">
        <div class="container">
            <div class="row mb--n30">
                <?php foreach ($arResult["ITEMS"] as $arItem): ?>
                            <div class="col-lg-6 col-md-6 mb-30">
                                <div class="banner__box border-radius-5 position-relative">
                                    <a class="display-block" href="<?= $arItem["PROPERTIES"]['LINK']['VALUE'] ?>"><img class="banner__box--thumbnail border-radius-5" src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>" alt="banner-img">
                                        <div class="banner__box--content">
                                            <h2 class="banner__box--content__title "><?= $arItem['NAME'] ?? ''; ?></h2>
                                            <p class="banner__box--content__desc"><?= $arItem['DETAIL_TEXT'] ?? ''; ?></p>
                                            <span class="banner__box--content__btn primary__btn <?= $arItem['DETAIL_TEXT'] ?? ''; ?>"><?= $arItem['PREVIEW_TEXT'] ?? ''; ?> </span>
                                        </div>
                                    </a>
                                </div>
                            </div>
                <? endforeach; ?>
            </div>
        </div>
    </section>
    <!-- End banner section -->
<?php endif; ?>