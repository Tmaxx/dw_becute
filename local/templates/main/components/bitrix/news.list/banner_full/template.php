<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>


<?php if ($arResult["ITEMS"]): ?>
    <!-- Start banner fullwidth section -->
    <section class="banner__fullwidth--section position-relative">
        <img class="banner__fullwidth--bg__thumbnail" src="<?= $arResult["ITEMS"][0]["DETAIL_PICTURE"]["SRC"] ?>" alt="img">
        <div class="banner__fullwidth--inner">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-6 col-md-6">
                        <div class="banner__fullwidth--thumbnail">
                            <img src="<?= $arResult["ITEMS"][0]["PREVIEW_PICTURE"]["SRC"] ?>" alt="img">
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6">
                        <div class="banner__fullwidth--content">
                            <h3 class="banner__fullwidth--content__subtitle"><?= $arResult["ITEMS"][0]['PREVIEW_TEXT'] ?? ''; ?></h3>
                            <h2 class="banner__fullwidth--content__title"><?= $arResult["ITEMS"][0]['NAME'] ?? ''; ?></h2>
                            <p class="banner__fullwidth--content__desc"><?= $arResult["ITEMS"][0]['DETAIL_TEXT'] ?? ''; ?></p>
                            <a class="banner__fullwidth--content__btn primary__btn" href="<?= $arResult["ITEMS"][0]["PROPERTIES"]['LINK']['VALUE'] ?>"><?= $arResult["ITEMS"][0]["PROPERTIES"]['LINK_TITLE']['VALUE'] ?></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End banner fullwidth section -->
<?php endif; ?>