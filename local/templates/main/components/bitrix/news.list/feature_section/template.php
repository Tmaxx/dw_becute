<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?php if ($arResult["ITEMS"]): ?>
    <section class="feature__section section--padding">
        <div class="container">
            <div class="feature__inner d-flex justify-content-between">
                <?php foreach($arResult["ITEMS"] as $arItem):?>
                    <div class="feature__items d-flex align-items-center">
                        <div class="feature__icon">
                            <img src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>" alt="img">
                        </div>
                        <div class="feature__content">
                            <h2 class="feature__content--title h3"><?= $arItem['NAME'] ??  ''; ?></h2>
                            <p class="feature__content--desc"><?= $arItem['PREVIEW_TEXT'] ?? ''; ?></p>
                        </div>
                    </div>
                <?php endforeach;?>
            </div>
        </div>
    </section>
<?php endif; ?>


</div>
