<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?// echo '<pre>';
//echo print_r($arResult);
//echo print_r($arParams);
//echo '</pre>';
//?>

<?php if ($arResult["ITEMS"]): ?>
<section class="hero__slider--section">
    <div class="hero__slider--activation swiper">

        <div class="swiper-wrapper">
            <?php foreach ($arResult["ITEMS"] as $arItem): ?>
                <div class="swiper-slide">
                    <div class="hero__slider--items">
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-6 col-md-6">
                                    <div class="slider__content">

                                        <h2 class="slider__maintitle text__primary h1"><?= $arItem["PROPERTIES"]['NAME_ITEM_SLIDER']['VALUE']['TEXT'] ?? ''; ?></h2>
                                        <p class="slider__desc"><?= $arItem['DETAIL_TEXT'] ?? ''; ?></p>
                                        <?php if (!empty($arItem["DISPLAY_PROPERTIES"]['LINK']['VALUE'])): ?>
                                            <a class="primary__btn slider__btn" href="<?= $arItem["PROPERTIES"]['LINK']['VALUE'] ?>">
                                                <?= $arItem['PREVIEW_TEXT'] ?? ''; ?>
                                            </a>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="hero__slider--layer">
                            <?php if (!empty($arItem["PREVIEW_PICTURE"]["SRC"])): ?>
                                <img class="slider__layer--img" src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>" alt="slider-img">
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
        <div class="slider__pagination swiper-pagination"></div>
    </div>
</section>
<?php endif; ?>
<?php
//echo "<pre>"; print_r(SITE_TEMPLATE_PATH );
//echo "</pre>";
//?>

