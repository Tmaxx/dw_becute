<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>


<?php if (!empty($arResult)): ?>
            <?php foreach ($arResult as $arItem): ?>
        <li class="footer__widget--menu__list"><a class="footer__widget--menu__text" href="<?= $arItem['LINK'] ?>"><?= $arItem['TEXT'] ?></a></li>

            <?php endforeach; ?>
<?php endif; ?>

