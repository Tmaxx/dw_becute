<?
/**
 * @global var $arResult
 */

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?
$currentIndex = 0;
$previousHasSubMenu = false;
$previousIsCatalog = false;
?>

<div class="header__menu d-none d-lg-block">
    <nav class="header__menu--navigation">

        <ul class="header__menu--wrapper d-flex">
            <? foreach ($arResult as $item) { ?>
                <? if ($item['DEPTH_LEVEL'] == 1) { ?>

                    <?
                    if ($currentIndex != $item['ITEM_INDEX']) {
                        $currentIndex = $item['ITEM_INDEX']; ?>

                        <? if ($previousHasSubMenu) { ?>
                            </ul>
                        <? } ?>
                        <? if ($previousIsCatalog) { ?>
                            </div>
                        <? } ?>

                        </li>
                    <? } ?>

                    <? if (empty($item['PARAMS']['is_catalog'])) { ?>
                    <li class="header__menu--items">
                    <? } else { ?>
                    <li class="header__menu--items mega__menu--items">
                    <? } ?>
                        <a class="header__menu--link <?= $item['SELECTED'] ? 'active' : ''?>" href="<?= $item['LINK'] ?>"><?= $item['TEXT'] ?>
                            <? if ($item['IS_PARENT']) { ?>
                                <svg class="menu__arrowdown--icon" xmlns="http://www.w3.org/2000/svg" width="12" height="7.41" viewBox="0 0 12 7.41">
                                    <path  d="M16.59,8.59,12,13.17,7.41,8.59,6,10l6,6,6-6Z" transform="translate(-6 -8.59)" fill="currentColor" opacity="0.7"/>
                                </svg>
                            <? } ?>
                        </a>
                        <? if ($item['IS_PARENT']) {
                            $previousHasSubMenu = true;
                            ?>

                            <? if (empty($item['PARAMS']['is_catalog'])) { ?>
                                <ul class="header__sub--menu">
                            <? } else {
                                $previousIsCatalog = true;
                                ?>
                                <div class="header__mega--menu__wrapper">
                                    <ul class="header__mega--menu d-flex">
                            <? } ?>


                        <? } else {
                            $previousHasSubMenu = false;
                            $previousIsCatalog = false;
                        } ?>
                <? } elseif ($item['DEPTH_LEVEL'] == 2) { ?>
                    <? if ($previousIsCatalog) { ?>
                        <li class="header__mega--sub__menu_li">
                        <a class="header__mega--sub__menu--title" href="<?= $item['LINK'] ?>"><?= $item['TEXT'] ?></a>

                    <? } else { ?>
                        <li class="header__sub--menu__items">
                        <a href="<?= $item['LINK'] ?>" class="header__sub--menu__link"><?= $item['TEXT'] ?></a>
                    <? } ?>

                    </li>
                <? } ?>
            <? } ?>
        </ul>
    </nav>
</div>
