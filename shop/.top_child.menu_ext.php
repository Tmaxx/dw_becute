<?php
$aMenuLinks = [
    [
        "Shop Left Sidebar",
        "/",
        [],
        [],
        "",
    ],
    [
        "Shop Right Sidebar",
        "/",
        [],
        [],
        "",
    ],
    [
        "Shop Grid",
        "/",
        [],
        [],
        "",
    ],
    [
        "Shop Grid List",
        "/",
        [],
        [],
        "",
    ],
    [
        "Shop List",
        "/",
        [],
        [],
        "",
    ]
];

global $APPLICATION;
$aMenuLinksExt = $APPLICATION->IncludeComponent(
    "bitrix:menu.sections",
    "",
    Array(
        "ID" => $_REQUEST["ID"],
        "IBLOCK_TYPE" => "catalog",
        "IBLOCK_ID" => '4',
        "SECTION_URL" => "/shop/#SECTION_CODE#/",
        "DEPTH_LEVEL" => "3",
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "3600"
    )
);
$aMenuLinks = array_merge($aMenuLinks, $aMenuLinksExt);
?>